" Leader key
let mapleader=" "

" enables syntax highlighting
syntax on

" better colors
set termguicolors

" tab stuff
set tabstop=2
set softtabstop=2
set expandtab

" enable autoindents
set smartindent

" number of spaces used for autoindents
set shiftwidth=4

" adds line numbers
set relativenumber
set number

" columns used for the line number
set numberwidth=4

" highlights the matched text pattern when searching
set incsearch
set nohlsearch

" open splits intuitively
set splitbelow
set splitright

" navigate buffers without losing unsaved work
set hidden

" start scrolling when 8 lines from top or bottom
set scrolloff=8

" disable swap file
set noswapfile
set nobackup

" Save undo history
set undofile
set undodir=~/.config/nvim/undodir

" Enable mouse support
set mouse=a

" case insensitive search unless capital letters are used
set ignorecase
set smartcase

" scrolloff, to keep the cursor, close to the center
set scrolloff=8

" noshowmode, to not to show the mode, as lightline is already showing it
set noshowmode

" signcolumn, to show diagnostics signs, etc. at the left
set signcolumn=number

" adding system clipboard support
set clipboard=unnamedplus

" ===================================
" P L U G I N S ! ! ! !
" ===================================

call plug#begin('~/.config/nvim/plugged')

" General
Plug 'kyazdani42/nvim-web-devicons'                " Devicons

Plug 'nvim-lualine/lualine.nvim'                   " Status line

Plug 'akinsho/bufferline.nvim'                     " Buffers

Plug 'machakann/vim-highlightedyank'               " Highlight yanked text

Plug 'kyazdani42/nvim-tree.lua'                    " File explorer


" Colorschemes
Plug 'gruvbox-community/gruvbox'
Plug 'sainnhe/gruvbox-material'
Plug 'folke/tokyonight.nvim', { 'branch': 'main' } 

" Telescope requires plenary to function
Plug 'nvim-lua/plenary.nvim'

" The main Telescope plugin
Plug 'nvim-telescope/telescope.nvim'

" An optional plugin recommended by Telescope docs
Plug 'nvim-telescope/telescope-fzf-native.nvim', {'do': 'make' }

" vim-fugitive
Plug 'tpope/vim-fugitive'

" gitsigns
Plug 'lewis6991/gitsigns.nvim'

" LSP
Plug 'neovim/nvim-lspconfig'

" Installer for LSP
Plug 'williamboman/nvim-lsp-installer'

" Autocompletion
Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'L3MON4D3/LuaSnip'
Plug 'saadparwaiz1/cmp_luasnip'
Plug 'onsails/lspkind-nvim'

" Treesitter
Plug 'nvim-treesitter/nvim-treesitter', { 'do': ':TSUpdate' }


" FloatTerm
Plug 'voldikss/vim-floaterm'

" Physics-motion in Vim
Plug 'yuttie/comfortable-motion.vim'

" Commentary for commenting code
Plug 'tpope/vim-commentary'

" Auto pairs for '(' '[' '{'
Plug 'jiangmiao/auto-pairs'

call plug#end()

" declaring the current colorscheme, which is to be used
colorscheme gruvbox-material
let g:gruvbox_material_background = 'medium'

" Use this for dark color schemes
set background=dark

" ===============================
" L U A  S T U F F ! ! !
" ===============================

lua require('tanushb')


" ===============================
" K E Y B I N D I N G S
" ===============================

nnoremap <leader>h :wincmd h<Cr>
nnoremap <leader>j :wincmd j<Cr>
nnoremap <leader>k :wincmd k<Cr>
nnoremap <leader>l :wincmd l<Cr>
